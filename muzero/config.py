import os
import gym
import torch
import shutil
from muzero.game import GameGym

class DiscreteSupport:
    def __init__(self, min_support: int, max_support: int):
        assert min_support < max_support
        self.min = min_support
        self.max = max_support
        self.range = range(min_support, max_support + 1)
        self.size = len(self.range)

class MuZeroConfig(object):
    def __init__(self,
                environment: gym.Env,
                training_steps: int,
                checkpoint_interval: int,
                window_size: int,
                max_moves: int,
                discount: float,
                dirichlet_alpha: float,
                num_simulations: int,
                batch_size: int,
                td_steps: int,
                num_actors: int,
                lr_init: float,
                lr_decay_rate: float,
                lr_decay_steps: float,
                value_support: DiscreteSupport,
                reward_support: DiscreteSupport):
        ### Self-Play
        self.env_name = environment.spec.id
        self.action_space_size = environment.action_space.n
        self.observation_space = environment.observation_space.shape
        self.num_actors = num_actors

        self.max_moves = max_moves
        self.num_simulations = num_simulations
        self.discount = discount

        # Root prior exploration noise.
        self.root_dirichlet_alpha = dirichlet_alpha
        self.root_exploration_fraction = 0.25

        # Network Architecture
        self.history = 4
        self.hidden_space_size = 32
        self.value_support = value_support
        self.reward_support = reward_support

        # UCB formula
        self.c_base = 19652
        self.c_init = 1.25

        # If we already have some information about which values occur in the
        # environment, we can use them to initialize the rescaling.
        # This is not strictly necessary, but establishes identical behaviour to
        # AlphaZero in board games.
        # self.known_bounds = known_bounds

        ### Training
        self.training_steps = training_steps
        self.checkpoint_interval = checkpoint_interval
        self.window_size = window_size
        self.batch_size = batch_size
        self.max_grad_norm = 5
        self.num_unroll_steps = 5
        self.td_steps = td_steps
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'

        # Logging and checkpoints.
        self.model_path = './model/'
        self.render_path = './render/'
        # self.log_path = './log/'
        if not os.path.isdir(self.model_path):
            os.mkdir(self.model_path)

        if os.path.isdir(self.render_path):
            shutil.rmtree(self.render_path)
        os.mkdir(self.render_path)
        # if not os.path.isdir(self.log_path):
        #     os.mkdir(self.log_path)

        self.weight_decay = 1e-4
        self.momentum = 0.9

        # Exponential learning rate schedule
        self.lr_init = lr_init
        self.lr_decay_rate = lr_decay_rate
        self.lr_decay_steps = lr_decay_steps

    def new_game(self, seed=None):
        env = gym.make(self.env_name)
        if seed is not None:
            env.seed(seed)
        return GameGym(env, self.discount, self.history) # making replica of environment.

    def visit_softmax_temperature(self, step_counter):
        if step_counter < 0.5 * self.training_steps:
            return 1.0
        elif step_counter < 0.75 * self.training_steps:
            return 0.5
        else:
            return 0.25

    @staticmethod
    def scalar_transformation(x: torch.Tensor):
        assert type(x) == torch.Tensor
        epsilon = 0.01
        sign = torch.ones(x.shape).float().to(x.device)
        sign[x < 0] = -1.0
        output = sign * (torch.sqrt(torch.abs(x) + 1) - 1 + epsilon * x)
        return output

    @staticmethod
    def inverse_scalar_transform(logits: torch.Tensor, scalar_support: DiscreteSupport):
        value_probs = torch.softmax(logits, dim=1)
        value_support = torch.ones(value_probs.shape)
        value_support[:, :] = torch.tensor([x for x in scalar_support.range])
        value_support = value_support.to(device=value_probs.device)
        value = (value_support * value_probs).sum(1, keepdim=True)

        epsilon = 0.001
        sign = torch.ones(value.shape).float().to(value.device)
        sign[value < 0] = -1.0
        output = (((torch.sqrt(1 + 4 * epsilon * (torch.abs(value) + 1 + epsilon)) - 1) / (2 * epsilon)) ** 2 - 1)
        output = sign * output
        return output

    def inverse_reward_transform(self, reward_logits):
        return self.inverse_scalar_transform(reward_logits, self.reward_support)

    def inverse_value_transform(self, value_logits):
        return self.inverse_scalar_transform(value_logits, self.value_support)

    @staticmethod
    def _phi(x: torch.Tensor, scalar_support: DiscreteSupport):
        x.clamp_(scalar_support.min, scalar_support.max)
        x_low = x.floor()
        x_high = x.ceil()
        p_high = x - x_low
        p_low = x_high - x

        target = torch.zeros(x.shape[0], x.shape[1], scalar_support.size).to(x.device)
        x_high_idx, x_low_idx = x_high - scalar_support.min, x_low - scalar_support.min
        target.scatter_(2, x_high_idx.long().unsqueeze(-1), p_high.unsqueeze(-1))
        target.scatter_(2, x_low_idx.long().unsqueeze(-1), p_low.unsqueeze(-1))
        return target

    def value_phi(self, x):
        return self._phi(x, self.value_support)

    def reward_phi(self, x):
        return self._phi(x, self.reward_support)


def make_gym_config(environment: gym.Env) -> MuZeroConfig:
    return MuZeroConfig(
        environment=environment,
        training_steps=40000,
        checkpoint_interval=20,
        window_size=2000,
        max_moves=500,
        discount=0.997,
        dirichlet_alpha=0.25,
        num_simulations=50,
        batch_size=512,
        td_steps=10,
        num_actors=16,
        lr_init=0.1,
        lr_decay_rate=0.01,
        lr_decay_steps=40000,
        value_support=DiscreteSupport(-40, 40),
        reward_support=DiscreteSupport(-10, 10))
