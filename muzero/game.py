import gym
import torch

from gym import wrappers
from typing import List

class GameGym(object):
    """A single episode of interaction with the environment."""
    def __init__(self, env: gym.Env, discount: float, history : int):
        self.env = env # Game specific environment.
        self.state_history = [self.env.reset()] # init with starting observation.
        self.action_history = []
        self.rewards = []
        self.child_visits = []
        self.root_values = []
        self.action_space_size = self.env.action_space.n
        self.discount = discount
        self.image_size = history
        self.done = False

    def terminal(self) -> bool:
        return self.done

    def legal_actions(self) -> List[int]:
        # Game specific calculation of legal actions.
        return [a for a in range(self.action_space_size)]

    def apply(self, action: int):
        state, reward, self.done, _ = self.env.step(action)
        self.rewards.append(reward)
        self.state_history.append(state)
        self.action_history.append(action)

    def store_search_statistics(self, root):
        sum_visits = sum(child.visit_count for child in root.children.values())
        action_space = (i for i in range(self.action_space_size))
        self.child_visits.append([
            root.children[a].visit_count / sum_visits if a in root.children else 0
            for a in action_space
        ])
        self.root_values.append(root.value())

    def make_image(self, state_index: int):
        # Game specific feature planes.
        image = self.state_history[:state_index][-self.image_size:]
        return (self.image_size - len(image)) * [self.state_history[0]] + image # padding with first state if needed.

    def make_target(self, state_index: int, num_unroll_steps: int, td_steps: int):
        # The value target is the discounted root value of the search tree N steps
        # into the future, plus the discounted sum of all rewards until then.
        target_value, target_reward, target_policy = [], [], []

        for current_index in range(state_index, state_index + num_unroll_steps + 1):
            bootstrap_index = current_index + td_steps
            if bootstrap_index < len(self.root_values):
                value = self.root_values[bootstrap_index] * self.discount ** td_steps
            else:
                value = 0

            for i, reward in enumerate(self.rewards[current_index:bootstrap_index]):
                value += reward * self.discount**i  # pytype: disable=unsupported-operands

            # For simplicity the network always predicts the most recently received
            # reward, even for the initial representation network where we already
            # know this reward.
            if 0 < current_index < len(self.rewards):
                last_reward = self.rewards[current_index]
            else:
                last_reward = 0

            if current_index < len(self.root_values):
                target_value.append(value)
                target_reward.append(last_reward)
                target_policy.append(self.child_visits[current_index])
            else:
                # States past the end of games are treated as absorbing states.
                target_value.append(0.)
                target_reward.append(last_reward)
                target_policy.append(torch.zeros(self.action_space_size))

        return target_value, target_reward, target_policy

    def render_run(self, path, index):
        self.env = wrappers.Monitor(self.env, path + 'render_%d' % index, force=True)
        self.state_history = [self.env.reset()]