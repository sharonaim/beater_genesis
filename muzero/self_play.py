import ray
import numpy as np
import torch

from muzero.mcts import Mcts, Node
from muzero.config import MuZeroConfig
from muzero.model import MuModel
from muzero.manage_utils import SharedStorage, ReplayBuffer


def select_action(config: MuZeroConfig, node: Node, temperature: int, deterministic=False):
    action_probs = np.zeros(config.action_space_size)

    for action, child in node.children.items():
        action_probs[child.action] = child.visit_count ** (1 / temperature)
    action_probs = action_probs / action_probs.sum()

    if deterministic:
        action = np.argmax(action_probs)
    else:
        action = np.random.choice(len(action_probs), p=action_probs)

    return action


@ray.remote
class MuActor:
    def __init__(self, rank, config : MuZeroConfig, shared_storage: SharedStorage, replay_buffer: ReplayBuffer):
        self.rank = rank
        self.config = config
        self.shared_storage = shared_storage
        self.replay_buffer = replay_buffer

    # Each self-play job is independent of all others; it takes the latest network
    # snapshot, produces a game and makes it available to the training job by
    # writing it to a shared replay buffer.
    def run(self):
        model = ray.get(self.shared_storage.make_uniform_network.remote())
        model.to('cpu')

        with torch.no_grad():
            while ray.get(self.shared_storage.get_counter.remote()) < self.config.training_steps:
                model.set_weights(ray.get(self.shared_storage.get_weights.remote()))
                model.eval()

                game = self.play_game(model)
                self.replay_buffer.save_game.remote(game)
                self.shared_storage.set_actor_log.remote(sum(game.rewards))

    # Each game is produced by starting at the initial board position, then
    # repeatedly executing a Monte Carlo Tree Search to generate moves until the end
    # of the game is reached.
    def play_game(self, network: MuModel):
        game = self.config.new_game(self.rank)
        _temperature = self.config.visit_softmax_temperature(
            ray.get(self.shared_storage.get_counter.remote()))

        while not game.terminal() and len(game.action_history) < self.config.max_moves:
            root = Node(None, None, None) # starting new search trajectory.

            state_image = game.make_image(len(game.state_history))
            state_image = torch.tensor(state_image, dtype=torch.float32).view(1, -1)
            network_output = network.initial_inference(state_image)
            root.expand(network_output.item(), game.legal_actions())
            self.add_exploration_noise(root)

            # We then run a Monte Carlo Tree Search using only action sequences and the
            # model learned by the network.
            Mcts.run_mcts(root, network, self.config)
            action = select_action(self.config, root, _temperature, deterministic=False)

            game.apply(action)
            game.store_search_statistics(root)

        game.env.close()
        return game

    def add_exploration_noise(self, root: Node):
        actions = list(root.children.keys())
        noise = np.random.dirichlet([self.config.root_dirichlet_alpha] * len(actions))
        frac = self.config.root_exploration_fraction
        for a, n in zip(actions, noise):
            root.children[a].prior = root.children[a].prior * (1 - frac) + n * frac
