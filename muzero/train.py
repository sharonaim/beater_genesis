import ray
import time
import torch
import typing

from torch import optim
from torch.optim.lr_scheduler import _LRScheduler
from torch.utils.tensorboard import SummaryWriter
from datetime import datetime

from muzero.model import MuModel
from muzero.self_play import MuActor, select_action
from muzero.mcts import Mcts, Node
from muzero.manage_utils import SharedStorage, ReplayBuffer
from muzero.config import MuZeroConfig


class LogData(typing.NamedTuple):
    value_loss: float
    reward_loss: float
    policy_loss: float
    buffer_size: int
    games_collected: int


class DecayLR(_LRScheduler):
    def __init__(self, optimizer, decay_steps, decay_rate=0.1, last_epoch=-1):
        self.step_size = decay_steps
        self.gamma = decay_rate
        super(DecayLR, self).__init__(optimizer, last_epoch)

    def get_lr(self):
        return [max(base_lr * self.gamma ** (self.last_epoch / self.step_size), base_lr * self.gamma)
                for base_lr in self.base_lrs]


def soft_update(target, source, tau):
    for target_param, param in zip(target.parameters(), source.parameters()):
        target_param.data.copy_(
            target_param.data * (1.0 - tau) + param.data * tau
        )


def categorical_loss(logits, target):
    return -(torch.log_softmax(logits, dim=1) * target).sum(1)


def update_weights(model: MuModel, optimizer: optim.SGD,
                   replay_buffer: ReplayBuffer, config: MuZeroConfig):
    image_batch, action_batch, target_value, target_reward, target_policy = \
        ray.get(replay_buffer.sample_batch.remote(config.num_unroll_steps, config.td_steps))

    image_batch = torch.tensor(image_batch).to(config.device).float().view(config.batch_size, -1)
    action_batch = torch.tensor(action_batch).to(config.device).unsqueeze(-1)
    target_value = torch.tensor(target_value).to(config.device)
    target_reward = torch.tensor(target_reward).to(config.device)
    target_policy = torch.tensor(target_policy).to(config.device)

    transformed_target_reward = config.scalar_transformation(target_reward)
    target_reward_phi = config.reward_phi(transformed_target_reward)
    transformed_target_value = config.scalar_transformation(target_value)
    target_value_phi = config.value_phi(transformed_target_value)

    value, _, policy_logits, hidden_state = model.initial_inference(image_batch)

    value_loss = categorical_loss(value, target_value_phi[:, 0])
    policy_loss = categorical_loss(policy_logits, target_policy[:, 0])
    reward_loss = torch.zeros(config.batch_size, device=config.device)

    gradient_scale = 1 / config.num_unroll_steps
    for step_i in range(config.num_unroll_steps):
        value, reward, policy_logits, hidden_state = model.recurrent_inference(hidden_state, action_batch[:, step_i])
        value_loss += categorical_loss(value, target_value_phi[:, step_i + 1])
        policy_loss += categorical_loss(policy_logits, target_policy[:, step_i + 1])
        reward_loss += categorical_loss(reward, target_reward_phi[:, step_i])
        hidden_state.register_hook(lambda grad: grad * 0.5)

    loss = (policy_loss + value_loss + reward_loss)
    loss.register_hook(lambda grad: grad * gradient_scale)
    loss = loss.mean()

    optimizer.zero_grad()
    loss.backward()
    torch.nn.utils.clip_grad_norm_(model.parameters(), config.max_grad_norm)
    optimizer.step()

    log_data = LogData(value_loss.mean().item(), reward_loss.mean().item(), policy_loss.mean().item(),
            ray.get(replay_buffer.size.remote()), ray.get(replay_buffer.total_collected.remote()))
    return log_data


def _log(summary_writer: SummaryWriter, step_count: int, log_data: LogData, lr, best_score):
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    loss = log_data.value_loss + log_data.reward_loss +log_data.policy_loss
    lr = lr[0]

    print("[%s][INFO] Step => %d \t\tLoss: %f  [Value Loss: %f\tReward Loss: %f\tPolicy Loss: %f]"
          "\n\t\t Game Episodes Collected: %d\t Buffer Size: %d\t Learning Rate: %f\t Best Score: %s"
          % (now, step_count, loss, log_data.value_loss, log_data.reward_loss,
             log_data.policy_loss, log_data.games_collected, log_data.buffer_size, lr, str(best_score)))

    summary_writer.add_scalar('train/loss', loss, step_count)
    summary_writer.add_scalar('train/value_loss', log_data.value_loss, step_count)
    summary_writer.add_scalar('train/reward_loss', log_data.reward_loss, step_count)
    summary_writer.add_scalar('train/policy_loss', log_data.policy_loss, step_count)
    if best_score is not None:
        summary_writer.add_scalar('train/best_score', best_score, step_count)
    summary_writer.add_scalar('train/lr', lr, step_count)


def _train(config: MuZeroConfig, shared_storage: SharedStorage, replay_buffer: ReplayBuffer):
    model = ray.get(shared_storage.make_uniform_network.remote()).to(config.device)
    model.train()

    optimizer = optim.SGD(model.parameters(), lr=config.lr_init,
                          momentum=config.momentum, weight_decay=config.weight_decay)
    lr_scheduler = DecayLR(optimizer, config.lr_decay_steps, config.lr_decay_rate)
    summary_writer = SummaryWriter()

    # wait for replay buffer to be non-empty
    while ray.get(replay_buffer.size.remote()) == 0:
        pass

    for step_count in range(config.training_steps):
        shared_storage.incr_counter.remote()

        if step_count % config.checkpoint_interval == 0:
            shared_storage.set_weights.remote(model.get_weights())

        log_data = update_weights(model, optimizer, replay_buffer, config)
        _log(summary_writer, step_count, log_data, lr_scheduler.get_lr(),
             ray.get(shared_storage.get_actor_log.remote()))
        lr_scheduler.step()

    shared_storage.set_weights.remote(model.get_weights())
    return model

def test(config: MuZeroConfig, model: MuModel, index: int, render: bool):
    with torch.no_grad():
        game = config.new_game()
        if render:
            game.render_run(config.render_path, index)
        temperature = config.visit_softmax_temperature(index)

        while not game.terminal():
            root = Node(None, None, None) # starting new search trajectory.

            image = game.make_image(len(game.state_history))
            state_image = torch.tensor(image, dtype=torch.float32).view(1, -1)
            network_output = model.initial_inference(state_image)

            root.expand(network_output.item(), game.legal_actions())
            Mcts.run_mcts(root, model, config)
            game.apply(select_action(config, root, temperature, deterministic=True)) # dummy counter.

        game.env.close()
        return sum(game.rewards)

@ray.remote
def _test(config: MuZeroConfig, shared_storage: SharedStorage):
    test_model = ray.get(shared_storage.make_uniform_network.remote()).to('cpu')
    best_test_score = float('-inf')

    while ray.get(shared_storage.get_counter.remote()) < config.training_steps:
        test_model.set_weights(ray.get(shared_storage.get_weights.remote()))
        test_model.eval()

        test_score = test(config, test_model, ray.get(shared_storage.get_counter.remote()), True)
        if test_score > best_test_score:
            best_test_score = test_score
            shared_storage.set_best.remote(best_test_score)
            torch.save(test_model.state_dict(), config.model_path + 'best_score.pt')

        time.sleep(120)


def train(config: MuZeroConfig):
    shared_storage = SharedStorage.remote(config, MuModel(config))
    replay_buffer = ReplayBuffer.remote(config)

    actors = [MuActor.remote(rank, config, shared_storage, replay_buffer)
                for rank in range(config.num_actors)]
    actor_handlers = [actor.run.remote() for actor in actors]
    actor_handlers += [_test.remote(config, shared_storage)]

    trained_model = _train(config, shared_storage, replay_buffer)
    ray.wait(actor_handlers, len(actor_handlers))

    return trained_model
