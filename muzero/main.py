import gym
import ray
import torch
import numpy as np

from muzero.config import make_gym_config
from muzero.train import train

ray.init(include_dashboard=False )
if __name__ == '__main__':
    seed = 0
    np.random.seed(seed)
    torch.manual_seed(seed)

    # config = make_gym_config(gym.make('CartPole-v1'))
    config = make_gym_config(gym.make('LunarLander-v2'))
    # config = make_gym_config(gym.make('MountainCar-v0'))
    model = train(config)

    torch.save(model.state_dict(), "./save/final.pt")
