import ray
import numpy as np

from muzero.model import MuModel
from muzero.config import MuZeroConfig
from muzero.game import GameGym

@ray.remote
class SharedStorage:
    def __init__(self, config: MuZeroConfig, model: MuModel):
        print("Training Model")
        print(model)
        self.model = model
        self.config = config
        self.step_counter = 0
        self.best = 0
        self.reward_log = []

    def get_weights(self):
        return self.model.get_weights()

    def set_weights(self, weights):
        return self.model.set_weights(weights)

    def incr_counter(self):
        self.step_counter += 1

    def get_counter(self):
        return self.step_counter

    def set_best(self, best):
        self.best = best

    def get_best(self):
        return self.best

    def set_actor_log(self, reward):
        self.reward_log.append(reward)

    def get_actor_log(self):
        reward = None
        if len(self.reward_log) > 0:
            reward = sum(self.reward_log) / len(self.reward_log)
            self.reward_log = []

        return reward

    def make_uniform_network(self) -> MuModel:
        return MuModel(self.config)

@ray.remote
class ReplayBuffer:
    def __init__(self, config: MuZeroConfig):
        self.window_size = config.window_size
        self.batch_size = config.batch_size
        self.buffer = []
        self.counter = 0

    def total_collected(self):
        return self.counter

    def size(self):
        return len(self.buffer)

    def save_game(self, game):
        if len(self.buffer) > self.window_size:
            self.buffer.pop(0)
        self.buffer.append(game)
        self.counter += 1

    def sample_batch(self, num_unroll_steps: int, td_steps: int):
        image_batch, action_batch, value_batch, reward_batch, policy_batch = [], [], [], [], []

        games = [self.sample_game() for _ in range(self.batch_size)]
        game_pos = [(g, self.sample_position(g)) for g in games]

        for game, idx in game_pos:
            actions = game.action_history[idx:idx + num_unroll_steps]
            # random action selection to complete num_unroll_steps.
            actions += [np.random.randint(0, game.action_space_size)
                        for _ in range(num_unroll_steps - len(actions))]

            image_batch.append(game.make_image(idx))
            action_batch.append(actions)
            value, reward, policy = game.make_target(idx, num_unroll_steps, td_steps)
            value_batch.append(value)
            reward_batch.append(reward)
            policy_batch.append(policy)

        return image_batch, action_batch, value_batch, reward_batch, policy_batch

    def sample_game(self) -> GameGym:
        return self.buffer[np.random.choice(len(self.buffer))]

    def sample_position(self, game) -> int:
        return np.random.choice(len(game.action_history))
