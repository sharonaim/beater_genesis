import math
import torch
from typing import List

from muzero.config import MuZeroConfig
from muzero.model import  MuModel, NetworkOutput

class MinMaxStats(object):
    """A class that holds the min-max values of the tree."""

    def __init__(self, min_value_bound=None, max_value_bound=None):
        self.maximum = min_value_bound if min_value_bound else -float('inf')
        self.minimum = max_value_bound if max_value_bound else float('inf')

    def update(self, value: float):
        self.maximum = max(self.maximum, value)
        self.minimum = min(self.minimum, value)

    def normalize(self, value: float) -> float:
        if self.maximum > self.minimum:
            # We normalize only when we have set the maximum and minimum values.
            return (value - self.minimum) / (self.maximum - self.minimum)
        return value

class Node:
    def __init__(self, parent, action: int, prior: float):
        self.parent = parent
        self.action = action
        self.prior = prior
        self.hidden_state = None
        self.reward = 0
        self.children = {}

        self.visit_count = 0
        self.value_sum = 0

    def expand(self, network_output: NetworkOutput, action_space: List[int]) -> float:
        value, self.reward, policy_logits, self.hidden_state = network_output
        policy = {a: math.exp(policy_logits[a]) for a in action_space}
        policy_sum = sum(policy.values())

        for action, policy in policy.items():
            self.children[action] = Node(self, action, policy / policy_sum)
        return value  # returning predicted value.

    def backpropagate(self, value: float, minmax_stats: MinMaxStats, discount):
        node = self
        while node is not None:
            node.value_sum += value
            node.visit_count += 1
            minmax_stats.update(node.value())

            value = node.reward + discount * value
            node = node.parent

    def expanded(self) -> bool:
        return len(self.children) > 0

    def value(self) -> float:
        if self.visit_count == 0:
            return 0
        return self.value_sum / self.visit_count


class Mcts:
    # Core Monte Carlo Tree Search algorithm.
    # To decide on an action, we run N simulations, always starting at the root of
    # the search tree and traversing the tree according to the UCB formula until we
    # reach a leaf node.
    @staticmethod
    def run_mcts(root: Node, net: MuModel, config: MuZeroConfig):
        minmax_stats = MinMaxStats()

        for _ in range(config.num_simulations):
            node = root
            while node.expanded():
                node = Mcts.select_child(node, config, minmax_stats)

            network_output = net.recurrent_inference(node.parent.hidden_state, torch.tensor([[node.action]]))
            predicted_value = node.expand(network_output.item(), [a for a in range(config.action_space_size)])
            node.backpropagate(predicted_value, minmax_stats, config.discount)

    # selecting the child of node with highest ucb score.
    @staticmethod
    def select_child(node: Node, config: MuZeroConfig, minmax_stats: MinMaxStats) -> Node:
        ucb_values = {}
        for action, child in node.children.items():
            score = Mcts.ucb_score(node, child, config, minmax_stats)
            ucb_values[action] = score
        action = max(ucb_values, key=ucb_values.get)  # getting key of max value.
        return node.children[action]  # returning child.

    # calculating the ucb score of node with it's parent.
    @staticmethod
    def ucb_score(parent: Node, child: Node, config: MuZeroConfig, minmax_stats: MinMaxStats) -> float:
        visit_score = math.log((parent.visit_count + config.c_base + 1) / config.c_base) + config.c_init
        visit_score *= math.sqrt(parent.visit_count) / (child.visit_count + 1)

        prior_score = visit_score * child.prior
        value_score = minmax_stats.normalize(child.value())

        return prior_score + value_score
