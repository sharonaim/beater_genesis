import torch
from torch import nn
import typing

from muzero.config import MuZeroConfig


class NetworkOutput(typing.NamedTuple):
    value: torch.Tensor
    reward: torch.Tensor
    policy_logits: torch.Tensor
    hidden_state: torch.Tensor

    def item(self):
        return self.value.cpu().item(), self.reward.cpu().item(),\
               self.policy_logits.squeeze(0).cpu().numpy(), self.hidden_state

class MuModel(nn.Module):
    rep : nn.Module
    dynamics : nn.Module
    predict : nn.Module

    def __init__(self, config : MuZeroConfig):
        super(MuModel, self).__init__()
        self.action_space_size = config.action_space_size
        self.inverse_value_transform = config.inverse_value_transform
        self.inverse_reward_transform = config.inverse_reward_transform

        self.represent = repModel(config.observation_space, config.hidden_space_size, config.history)
        self.dynamics = dynamicsModel(self.action_space_size, config.hidden_space_size, config.reward_support.size)
        self.predict = predictModel(self.action_space_size, config.hidden_space_size, config.value_support.size)

    def initial_inference(self, image) -> NetworkOutput:
        hidden_state = self.represent(image)
        policy_logits, value = self.predict(hidden_state)

        if not self.training:
            value = self.inverse_value_transform(value)

        return NetworkOutput(value, torch.zeros(image.shape[0], 1, device=image.device), policy_logits, hidden_state)

    def recurrent_inference(self, hidden_state: torch.Tensor, action) -> NetworkOutput:
        action_tensor = torch.zeros(size=(action.shape[0], self.action_space_size), device=hidden_state.device)
        action_tensor.scatter_(1, action, 1.0)

        state, reward = self.dynamics(hidden_state, action_tensor)
        policy_logits, value = self.predict(state)

        if not self.training:
            value = self.inverse_value_transform(value)
            reward = self.inverse_reward_transform(reward)

        return NetworkOutput(value, reward, policy_logits, state)

    def get_weights(self):
        return {k: v.cpu() for k, v in self.state_dict().items()}

    def set_weights(self, weights):
        self.load_state_dict(weights)

    def get_gradients(self):
        grads = []
        for p in self.parameters():
            grad = None if p.grad is None else p.grad.data.cpu().numpy()
            grads.append(grad)
        return grads

    def set_gradients(self, gradients):
        for g, p in zip(gradients, self.parameters()):
            if g is not None:
                p.grad = torch.from_numpy(g)


class repModel(nn.Module):
    def __init__(self, obs_space, hidden_space_size, history):
        super(repModel, self).__init__()
        self.input_size = 1
        for i in obs_space:
            self.input_size *= i

        self.image_size = self.input_size * history
        self.hidden_space_size = hidden_space_size

        self.rep = nn.Sequential(
            nn.Linear(self.image_size, 128),
            nn.Tanh(),
            nn.Linear(128, hidden_space_size),
            nn.Tanh()
        )

    def forward(self, x):
        x = self.rep(x)
        return x


class dynamicsModel(nn.Module):
    def __init__(self, action_space_size, hidden_space_size, reward_support_size):
        super(dynamicsModel, self).__init__()
        self.hidden_space_size = hidden_space_size
        self.input_size = self.hidden_space_size + action_space_size

        self.state_head = nn.Sequential(
            nn.Linear(self.input_size, 128),
            nn.Tanh(),
            nn.Linear(128, hidden_space_size),
            nn.Tanh()
        )

        self.reward_head = nn.Sequential(
            nn.Linear(self.input_size, 128),
            nn.LeakyReLU(),
            nn.Linear(128, reward_support_size)
        )

    def forward(self, state, action):
        x = torch.cat((state, action), -1)

        next_state = self.state_head(x)
        reward = self.reward_head(x)
        return next_state, reward


class predictModel(nn.Module):
    def __init__(self, action_space_size, hidden_space_size, value_support_size):
        super(predictModel, self).__init__()

        self.policy_head = nn.Sequential(
            nn.Linear(hidden_space_size, 128),
            nn.LeakyReLU(),
            nn.Linear(128, action_space_size),
        )

        self.value_head = nn.Sequential(
            nn.Linear(hidden_space_size, 128),
            nn.LeakyReLU(),
            nn.Linear(128, value_support_size)
        )

    def forward(self, state):
        policy = self.policy_head(state)
        value = self.value_head(state)

        return policy, value
